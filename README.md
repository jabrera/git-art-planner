# git-art-planner
Generates dates when to contribute to highlight specific dates in your contribution calendar. If you use this, it doesn't mean you should not contribute for the blank days. You can still contribute but not as much as the highlighted ones if you really want to emphasize the text in your contribution calendar.

![image](https://user-images.githubusercontent.com/13174418/186624068-db40d679-2754-400b-b3af-909592df86c0.png)

## Live Demo
You can access the live demo [here](https://projects.juvarabrera.com/git-art-planner).

## How to run yourself
1. Just `git clone` the repository
2. Open `index.html` to any browser. No need to serve in a localhost.

## How to use
1. Enter the text you want to print in the contribution calendar.
2. Enter what date you want start printing your text. By default, the date is the current date.

## Contribute
1. Go to `characters.js`
2. Insert new letter (String) as key with a 2D array with values `0` or `1`.
3. The values will be plotted in the calendar with value `1` indicating that it will be highlighted in the contribution calendar.

You can also use the `Debug Mode` to click on the boxes and it will generate the values for you.
![image](https://user-images.githubusercontent.com/13174418/186624391-44272883-6736-4909-ac45-01e986eaf840.png)


## Sample letter
`a`
```
"a": [
  [0,0,0,1,1,0,0],
  [0,0,1,0,0,1,0],
  [0,0,1,1,1,0,0],
  [0,0,0,0,0,1,0]
]
```

Each array is equivalent to 1 week or 1 vertical line in the contribution calendar. Each value in that array is equivalent to the days of that week from Sunday (index 0) to Saturday (index 6)

## Limitations
The contribution calendar is currently limited to exactly 1 year after the start date.
