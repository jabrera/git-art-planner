const contributionPlanner = document.getElementById("contribution-planner");
const txtText = document.getElementById("txtText");
const txtDate = document.getElementById("txtDate");
const letterError = document.getElementById("letterError");
const dateError = document.getElementById("dateError");
const debugMode = document.getElementById("debugMode");
const datesContribute = document.getElementById("datesContribute");
const btnDownloadIcs = document.getElementById("btnDownloadIcs");
const downloadIcsContainer = document.getElementById("downloadIcsContainer");
const timeReminder = document.getElementById("timeReminder");
const form = document.getElementById("form");
const characterValue = document.getElementById("characterValue");
const gitArtPlanner = new GitArtPlanner(contributionPlanner, characterValue);

debugMode.addEventListener("change", event => {
    document.querySelectorAll("[data-mode]").forEach(el => {
        el.style.display = "none";
    })
    if (event.target.checked) {
        document.querySelector("[data-mode='debug']").style.display = "block";
        gitArtPlanner.debug(true);
    } else {
        document.querySelector("[data-mode='input']").style.display = "block";
        gitArtPlanner.debug(false);
    }
});
let changeDebugMode = new Event("change");
debugMode.dispatchEvent(changeDebugMode);
txtText.addEventListener("keyup", event => {
    let text = event.target.value;
    let output = gitArtPlanner.renderText(text);
    letterError.innerHTML = "";
    if (output.errorLetters.length != 0) {
        letterError.innerHTML = `The following characters is not supported: `;
        output.errorLetters.forEach(letter => {
            letterError.innerHTML += `<span class="tag">${letter}</span>`;
        });
    }
    datesContribute.innerHTML = "";
    if (output.dates.length != 0) {
        output.dates.forEach(date => {
            datesContribute.innerHTML += `<span class="tag">${date}</span>`;
        });
        downloadIcsContainer.style.display = "block";
    }
    else {
        downloadIcsContainer.style.display = "none";
    }
});
const generateICS = (events) => {
    let icsContent = `BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//Your Company//Your App//EN
CALSCALE:GREGORIAN\n\n`;
    const text = txtText.value;
    const time = timeReminder.value.replace(":","");
    events.forEach(date => {
        date = date.split("-").join("");
        const startDate = `${date}T${time}00`; // Start of the day
        const endDate = `${date}T${time}00`;   // End of the day
        icsContent += `BEGIN:VEVENT
UID:${Math.random().toString(36).substr(2, 10)}@git-art-planner.projects.juvarabrera.com
DTSTART:${startDate}
DTEND:${endDate}
SUMMARY:git push - ${text}
DESCRIPTION:
LOCATION:
END:VEVENT\n\n`;
    });
    icsContent += 'END:VCALENDAR';
    return icsContent;
}
const downloadICSFile = (icsContent) => {
    const blob = new Blob([icsContent], { type: 'text/calendar;charset=utf-8' });
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'events.ics');
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
timeReminder.addEventListener("input", event => {
    btnDownloadIcs.removeAttribute("disabled");
})
btnDownloadIcs.addEventListener("click", event => {
    const text = txtText.value;
    const output = gitArtPlanner.renderText(text);
    const icsContent = generateICS(output.dates);
    downloadICSFile(icsContent);
    
})
txtDate.addEventListener("keyup", event => {
    let dateString = event.target.value;
    let dateElements = dateString.split("-");
    dateError.innerHTML = '';
    let date = new Date(parseInt(dateElements[0]), parseInt(dateElements[1]) - 1, dateElements[2]);
    if (dateString == "") date = new Date();
    if (date instanceof Date && !isNaN(date.valueOf())) {
        gitArtPlanner.renderBlank(date);
        let keyupEvent = new Event('keyup');
        txtText.dispatchEvent(keyupEvent);
    } else {
        dateError.innerHTML = 'Cannot parse date. Please use YYYY-MM-DD format';
    }
});