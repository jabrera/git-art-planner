class GitArtPlanner {
    constructor(container, txtCharacterValue) {
        this.container = container;
        this.renderBlank();
        this.characters = CHARACTERS;
        this.separatorBetweenLetters = CHARACTERS["separator"];
        this.is_debug = false;
        this.text = "";
        this.date = new Date();
        this.txtCharacterValue = txtCharacterValue;
    }
    debug(is_debug = false) {
        this.is_debug = is_debug;
        this.txtCharacterValue.value = "";
        this.container.querySelectorAll(".contribution-day").forEach(dayElement => {
            dayElement.classList.remove("highlight");
        });
        this.container.classList.remove("debug");
        if(!is_debug) {
            this.renderBlank(this.date);
            this.renderText(this.text);
            return;
        };
        this.container.classList.add("debug");
    }
    getCharacterValue() {
        let characterValue = [
        ];
        let lastIndexWithValue = -1;
        document.querySelectorAll(".contribution-week").forEach(week => {
            if(week.querySelector(".transparent") != null) return;
            let arr = [];
            let hasValue = false;
            week.querySelectorAll("[data-date]").forEach((day, dayIndex) => {
                if (day.classList.contains("highlight")) {
                    arr.push(1);
                    hasValue = true;
                } else {
                    arr.push(0);
                }
            });
            if (!hasValue && lastIndexWithValue == -1) return;
            if (hasValue) lastIndexWithValue = characterValue.length; 
            characterValue.push(arr)
        });
        characterValue.splice(lastIndexWithValue+1-characterValue.length);
        let arrString = `"character-name": [ // 1 character only `;
        characterValue.forEach(arr => {
            arrString += `\n\t[${arr.toString()}],`
        })
        arrString = arrString.slice(0, -1);
        arrString += "\n]";
        this.txtCharacterValue.value = arrString;
    }
    renderText(text = "") {
        this.text = text;
        let obj = this;
        let highlights = [];
        let errorLetters = [];
        // if (text == "") return;
        let letters = text.split("");
        let dates = [];
        
        letters.forEach((letter, letterIndex) => {
            if (!obj.characters.hasOwnProperty(letter) && errorLetters.indexOf(letter) == -1) {
                errorLetters.push(letter);
                return;
            }
            if (obj.characters[letter].length == 0 && errorLetters.indexOf(letter) == -1) {
                errorLetters.push(letter);
                return;
            }
            highlights.push(...obj.characters[letter]);
            if (letterIndex != letters.length)
                highlights.push(...obj.separatorBetweenLetters);
        });
        this.container.querySelectorAll("[data-date]").forEach(day => {
            day.classList.remove("highlight");
        })
        document.querySelectorAll(".contribution-week").forEach(week => {
            if(week.querySelector(".transparent") != null) return;
            if(highlights.length == 0) return; 

            let highlight = highlights[0];
            week.querySelectorAll("[data-date]").forEach((day, dayIndex) => {
                if (highlight[dayIndex] == 1) {
                    day.classList.add("highlight");
                    dates.push(day.getAttribute("data-date"));
                }
            });
            highlights.shift();
        });

        return {
            "errorLetters": errorLetters,
            "dates": dates
        }
    }
    renderBlank(today = new Date()) {
        this.date = new Date(today.getFullYear(), today.getMonth(), today.getDate());
        this.container.innerHTML = "";
        let inOneYear = new Date(today.getFullYear()+1, today.getMonth(), today.getDate() - 1);
        let week = null;
        let hasLastWeek = false;
        let yearMainContainer = document.createElement("div");
        yearMainContainer.classList.add("contribution-year");
        this.container.insertAdjacentElement("beforeend",yearMainContainer);

        let yearContainer = document.createElement("div");
        yearContainer.classList.add("contribution-calendar");
        yearMainContainer.insertAdjacentElement("beforeend",yearContainer);

        let dayLabel = document.createElement("div");
        dayLabel.classList.add("contribution-day-label");
        let weekLabel = document.createElement("div");
        weekLabel.classList.add("contribution-week-label");
        dayLabel.insertAdjacentElement("beforeend",weekLabel);
        ["","M","","W","","F",""].forEach(dayString => {
            let dayDiv = document.createElement("div");
            dayDiv.textContent = dayString;
            dayLabel.insertAdjacentElement("beforeend",dayDiv);
        });
        yearContainer.insertAdjacentElement("beforeend",dayLabel);
        while (true) {
            let dateString = today.getFullYear() + "-" + ((today.getMonth() + 1) < 10 ? "0"+(today.getMonth() + 1) : (today.getMonth() + 1)) + "-" + (today.getDate() < 10 ? "0" + today.getDate() : today.getDate());
            if (today.getDay() == 0 && week != null) {
                hasLastWeek = false;
                yearContainer.insertAdjacentElement("beforeend",week);
            }
            if (week == null || today.getDay() == 0) {
                week = document.createElement("div");
                week.classList.add("contribution-week");
                let week_label = document.createElement("div");
                week_label.classList.add("contribution-week-label");
                let next7Days = new Date(today.getTime());
                next7Days.setDate(today.getDate() + (6 - today.getDay()));
                if ((today.getMonth() == next7Days.getMonth() && today.getDate() <= 7 && next7Days.getDate() <= 7) || 
                    today.getMonth() != next7Days.getMonth()) {
                    week_label.textContent = next7Days.toLocaleDateString("en-us", { "month": "short"})
                }
                week.insertAdjacentElement("beforeend", week_label);
                for (let i = 0; i < today.getDay(); i++) {
                    let day = document.createElement("div");
                    day.classList.add("contribution-day");
                    day.classList.add("transparent");
                    let dateString2 = today.getFullYear() + "-" + ((today.getMonth() + 1) < 10 ? "0"+(today.getMonth() + 1) : (today.getMonth() + 1)) + "-" + (today.getDate() < 10 ? "0" + today.getDate() : today.getDate());
                    week.insertAdjacentElement("beforeend", day);
                }
                hasLastWeek = true;
            }
            let day = document.createElement("div");
            day.classList.add("contribution-day");
            day.setAttribute("data-date", dateString);
            let dayTooltip = document.createElement("div");
            dayTooltip.classList.add("contribution-day-tooltip");
            dayTooltip.textContent = dateString;
            day.insertAdjacentElement("beforeend", dayTooltip);
            // day.textContent = oneYearAgo.getDay();
            week.insertAdjacentElement("beforeend", day);
            if (!(inOneYear.getFullYear() != today.getFullYear() ||
            inOneYear.getMonth() != today.getMonth() ||
            inOneYear.getDate() != today.getDate())) break;
            today.setDate(today.getDate() + 1);
        }
        if (hasLastWeek) {
            yearContainer.insertAdjacentElement("beforeend",week);
        }

        let obj = this;
        this.container.querySelectorAll(".contribution-day").forEach(dayElement => {
            dayElement.addEventListener("click", () => {
                if (!obj.is_debug) return;
                dayElement.classList.toggle("highlight");
                obj.getCharacterValue();
            })
        })
    }
    downloadIcsFile() {
        
    }
}